package com.devcamp.s50.task5620.restapi.controller;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.s50.task5620.restapi.model.CDrink;

@RestController
public class CDrinkController {
    @CrossOrigin
    @GetMapping("/devcamp-drinks")
    //Khai báo danh sách
    public ArrayList<CDrink> getDrink(){
        ArrayList<CDrink> listDrink = new ArrayList<CDrink>();
        LocalDate today = LocalDate.now(ZoneId.systemDefault());
        //Khởi tạo đối tượng
        CDrink tratac = new CDrink(1,"TRATAC","Trà Tắc", today, today,10000);
        CDrink coca = new CDrink(2,"COCA","Cocacola", today, today,15000);
        CDrink pepsi = new CDrink(3,"PEPSI","Pepsi", today, today,15000);
        CDrink lavi = new CDrink(3,"LAVI","Lavile", today, today,10000);
        listDrink.add(tratac);
        listDrink.add(coca);
        listDrink.add(pepsi);
        listDrink.add(lavi);
        return listDrink;
    }
}
