package com.devcamp.s50.task5620.restapi.model;

import java.time.LocalDate;

public class CDrink {
     int id;
     String maNuocUong;
     String tenNuocUong;
     LocalDate ngayTao;
     LocalDate ngayCapNhat;
     int donGia;

    //Khởi tạo có đầy đủ tham số
    public CDrink(int id, String maNuocUong, String tenNuocUong, LocalDate ngayTao, LocalDate ngayCapNhat, int donGia) {
        this.maNuocUong = maNuocUong ;
        this.tenNuocUong = tenNuocUong;
        this.ngayTao = ngayTao;
        this.ngayCapNhat = ngayCapNhat;
        this.donGia = donGia;
    }

    public void setMaNuocUong(String maNuocUong){
        this.maNuocUong = maNuocUong ;
    }

    public String getMaNuocUong(){
        return maNuocUong;
    }

    public void setTenNuocUong(String tenNuocUong){
        this.tenNuocUong = tenNuocUong ;
    }

    public String getTenNuocUong(){
        return tenNuocUong;
    }

    public void setNgayTao(LocalDate ngayTao){
        this.ngayTao = ngayTao ;
    }

    public LocalDate getNgayTao(){
        return ngayTao;
    }

    public void setNgayCapNhat(LocalDate ngayCapNhat){
        this.ngayCapNhat = ngayCapNhat ;
    }

    public LocalDate getNgayCapNhat(){
        return ngayCapNhat;
    }

    public void setDonGia(int donGia){
        this.donGia = donGia ;
    }

    public int getDonGia(){
        return donGia;
    }


    //Khởi tạo không tham số
    public CDrink() {
        System.out.println("Không co tham so");
    }

    @Override
    public String toString(){
        return "maNuocUong : " + this.maNuocUong + ", tenNuocUong : " + this.tenNuocUong + ", ngayTao : " + this.ngayTao
        + ", ngayCapNhat : " + this.ngayCapNhat + ", donGia : " + this.donGia;
    }

}